from PyQt5.QtCore import pyqtSignal, QPointF, QSize, Qt
from PyQt5.QtGui import QPainter, QPolygonF, QPixmap
from PyQt5.QtWidgets import (QAbstractItemView, QApplication, QStyle,
        QStyledItemDelegate, QTableWidget, QTableWidgetItem, QWidget)
from PyQt5 import QtWidgets, QtCore, QtGui
import os
import collections
import functools

from module.dbAccess import dbAccess

class MyListView(QtWidgets.QTreeView):
    pass

class Mediated:

    def __init__(self):
        self.mediator = None


    def on_change(self):
        if self.mediator is not None:
            self.mediator.send(self)

class MyPushButton(QtWidgets.QPushButton, Mediated):
    def __init__(self, *__args):
        super(MyPushButton, self).__init__(*__args)


class MyWidget(QtWidgets.QWidget):
    def __init__(self, parent=None, db_path=None):
        super(MyWidget, self).__init__(parent)
        if os.path.exists(db_path) is False:
            raise FileNotFoundError(db_path)

        self.setup_ui()
        self.create_mediator()

        self.db_access = dbAccess(db_name=db_path)
        self.connection = self.db_access.connection

        self.controller = Controller()
        self.controller.Hello()


    def setup_ui(self):
        lblAuthor = QtWidgets.QLabel("作者")
        self.leAuthor = QtWidgets.QLineEdit()
        lblAuthor.setBuddy(self.leAuthor)
        btnAuthor = QtWidgets.QPushButton("クリア")
        btnAuthor.clicked.connect(self.leAuthor.clear)

        lblTitle = QtWidgets.QLabel("タイトル")
        self.leTitle = QtWidgets.QLineEdit()
        self.leTitle.textChanged.connect(self.titleChanged)
        lblTitle.setBuddy(self.leTitle)
        btnTitle = QtWidgets.QPushButton("クリア")
        btnTitle.clicked.connect(self.leTitle.clear)

        lblGenre = QtWidgets.QLabel("ジャンル")
        self.cbGenre = QtWidgets.QComboBox()
        self.cbGenre.setEditable(True)
        lblGenre.setBuddy(self.cbGenre)
        btnGenre = QtWidgets.QPushButton("クリア")
        # Todo ボタンをクリックするとIndexを0にする
        # btnGenre.clicked.connect(self.cbGenre.setCurrentIndex(0))
        # TypeError: argument 1 has unexpected type 'NoneType'

        self.lvResult = MyListView()

        self.btnExecute = MyPushButton("実行") #QtWidgets.QPushButton("実行")
        # btnExecute.clicked.connect(self.call_controller)
        self.btnExecute.clicked.connect(self.execute_query)
        self.btnCancel = QtWidgets.QPushButton("戻る")
        # btnCancel.clicked.connect(self.close)
        spacer = QtWidgets.QSpacerItem(10, 10, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        layoutButton = QtWidgets.QHBoxLayout()
        layoutButton.addWidget(self.btnExecute)
        layoutButton.addWidget(self.btnCancel)

        gridLayout = QtWidgets.QGridLayout()
        gridLayout.addWidget(lblAuthor, 0, 0)
        gridLayout.addWidget(self.leAuthor, 0, 1)
        gridLayout.addWidget(btnAuthor, 0, 2)
        gridLayout.addWidget(lblTitle, 1, 0)
        gridLayout.addWidget(self.leTitle, 1, 1)
        gridLayout.addWidget(btnTitle, 1, 2)
        gridLayout.addWidget(lblGenre, 2, 0)
        gridLayout.addWidget(self.cbGenre, 2, 1)
        gridLayout.addWidget(btnGenre, 2, 2)
        gridLayout.addWidget(self.lvResult, 3, 0, 1, 3)

        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addLayout(gridLayout)
        mainLayout.addLayout(layoutButton)
        self.setLayout(mainLayout)

    def create_mediator(self):
        self.mediator = self._query_ui_mediator(self._clicked_mediator())
        for widget in (self.leAuthor, self.leTitle, self.cbGenre, self.btnExecute, self.btnCancel):
            widget.mediator = self.mediator
        self.mediator.send(None)

    # def coroutine(function):
    #     @functools.wraps(function)
    #     def wrapper(*args, **kwargs):
    #         generator = function(*args, **kwargs)
    #         next(generator)
    #         return generator
    #     return wrapper

    def coroutine(function):
        @functools.wraps(function)
        def wrapper(*args, **kwargs):
            generator = function(*args, **kwargs)
            next(generator)
            return generator

        return wrapper

    @coroutine
    def _query_ui_mediator(self, successor=None):
        print("query_ui_mediator")
        while(True):
            widget = (yield)
            if widget == self.leAuthor:
                print("leAuthor")
            elif widget == self.leTitle:
                print("leTitle")
            elif widget == self.cbGenre:
                print("cbGenre")
            elif successor is not None:
                successor.send(widget)


    @coroutine
    def _clicked_mediator(self, successor=None):
        print("clicked_mediator")
        while(True):
            widget = (yield)
            if widget == self.btnExecute:
                print("btnExecute")
            elif widget == self.btnCancel:
                self.close()
            elif successor is not None:
                successor.send(widget)


    def update_query(self):
        print("call update_query")

    def execute_query(self, **kwargs):
        print("execute_query")
        author = kwargs.get("author")
        title = kwargs.get("title")
        genre = kwargs.get("genre")
        # result = dbAccess.get_record_by_title(self.connection, "小学生")

        # model = QtGui.QStandardItemModel()
        # for r in result:
        #     item = QtGui.QStandardItem()
        #     item.setText(r.get("title"))
        #     model.appendRow(item)
        # self.lvResult.setModel(model)

    def titleChanged(self, text):
        self.call_controller(text)

    def keyPressEvent(self, QKeyEvent):
        if QKeyEvent.key() == QtCore.Qt.Key_Escape:
            self.close()

    def call_controller(self, title):
        if title.strip() == "":
            return
        model = self.controller.execute_query(self.connection, title=title)
        self.lvResult.setModel(model)


class Controller():
    def Hello(self, **kwargs):
        print("Hello")

        if kwargs.get("title") is not None:
            pass

    def execute_query(self, connection, **kwargs):
        if kwargs.get("title") is None:
            return
        print("execute_query")
        author = kwargs.get("author")
        title = kwargs.get("title")
        genre = kwargs.get("genre")
        result = dbAccess.get_record(connection, title=title)

        model = QtGui.QStandardItemModel()
        for r in result:
            item = QtGui.QStandardItem()
            item.setText(r.get("title"))
            model.appendRow(item)

        return model



if __name__ == "__main__":
    import sys
    app = QApplication(sys.argv)
    widget = MyWidget(db_path="/Users/Shady/doujin_books.db")
    widget.resize(QSize(720, 600))
    widget.show()
    sys.exit(app.exec_())
