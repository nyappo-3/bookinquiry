import os
import sqlite3
import collections

class dbAccess():
    def __init__(self, **kwargs):
        db_name = kwargs.get("db_name")
        if os.path.exists(db_name) is False:
            raise(FileNotFoundError)

        self.__connection = sqlite3.connect(db_name)
        self.__connection.row_factory = sqlite3.Row
        assert(self.__connection is not None)

    # def __del__(self):
    #     if self.connection is not None:
    #         self.connection.close()

    @property
    def connection(self):
        return self.__connection


    def get_record(connection, author="", title="", genre=""):
        # result = connection.execute(
        #     "select author as author, title as title, genre as genre, path as path from books where \
        #     title like ? order by author, title", ("%" + title + "%",))
        # sql = 'select author, title, genre, path from books where \
        #     title like ? \
        #     order by author, title'
        # t = ("%" + title + "%",)

        sql = 'select author, title, genre, path from books where \
            author like ? AND title like ? AND genre like ?\
            order by author, title'

        # author = "" #""GRAPHICAROSSA"
        t = ('%' + author + '%', '%' + title + '%', '%' + genre + '%',)
        result = connection.execute(sql, t)

        r = []
        for row in result:
            dic = {"author": row["author"], "title": row["title"], "genre": row["genre"], "path": row["path"]}
            r.append(dic)

        return r

if __name__ == "__main__":
    db_access = dbAccess(db_name="/Users/Shady/doujin_books.db")
    conn = db_access.connection
    result = dbAccess.get_record(conn, title="orz", genre="ダンジョン")
    print(len(result), result)

    # self.db_access = dbAccess(db_name=db_path)
    # self.connection = self.db_access.connection
    # result = dbAccess.get_record_by_title(connection, title)
